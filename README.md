<div align="center"><h1>pre-commit-update [archived]</h1>

<strong>As of v0.3.1 project repo path is located <a href="https://gitlab.com/vojko.pribudic.foss/pre-commit-update" target="_blank">here</a></strong><br />
<strong>Migration, if used via pre-commit-hook, should be done automatically, however if that is not the case, please migrate manually.</strong>
</div>